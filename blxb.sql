-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2017-12-02 06:32:29
-- 服务器版本： 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blxb`
--

-- --------------------------------------------------------

--
-- 表的结构 `blxbgame_morning`
--

CREATE TABLE `blxbgame_morning` (
  `id` int(11) NOT NULL COMMENT '起床表序号',
  `userid` int(11) NOT NULL COMMENT '用户序号',
  `time` datetime NOT NULL COMMENT '签到时间',
  `msectime` int(10) NOT NULL DEFAULT '0' COMMENT '签到精确时间',
  `signdays` int(10) NOT NULL DEFAULT '0' COMMENT '连续签到天数',
  `totalscore` int(10) NOT NULL DEFAULT '0' COMMENT '总分'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='起床总表';

--
-- 转存表中的数据 `blxbgame_morning`
--

INSERT INTO `blxbgame_morning` (`id`, `userid`, `time`, `msectime`, `signdays`, `totalscore`) VALUES
(1, 1, '2017-12-01 22:53:09', 966068, 1, 108),
(2, 3, '2017-11-28 16:28:44', 793012, 1, 12),
(3, 4, '2017-12-01 22:53:23', 527923, 1, 60),
(4, 2, '2017-12-01 22:47:08', 682877, 1, 198),
(5, 5, '2017-12-01 23:24:53', 137372, 1, 12),
(6, 6, '2017-12-01 23:25:02', 616156, 1, 12),
(7, 7, '2017-12-01 23:25:11', 193521, 1, 12),
(8, 8, '2017-12-01 23:25:20', 555650, 1, 12),
(9, 9, '2017-12-01 23:25:47', 691954, 1, 12),
(10, 11, '2017-12-01 23:25:55', 546735, 1, 12),
(11, 14, '2017-12-01 23:26:13', 556043, 1, 12),
(12, 13, '2017-12-01 23:26:24', 593177, 1, 12);

-- --------------------------------------------------------

--
-- 表的结构 `blxbgame_morning_record`
--

CREATE TABLE `blxbgame_morning_record` (
  `id` int(250) NOT NULL COMMENT '起床记录序号',
  `userid` int(11) NOT NULL COMMENT '用户序号',
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '签到时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='起床记录';

--
-- 转存表中的数据 `blxbgame_morning_record`
--

INSERT INTO `blxbgame_morning_record` (`id`, `userid`, `time`) VALUES
(31, 2, '2017-11-29 16:34:58'),
(32, 2, '2017-12-01 22:47:08'),
(33, 1, '2017-12-01 22:53:09'),
(34, 4, '2017-12-01 22:53:23'),
(35, 5, '2017-12-01 23:24:53'),
(36, 6, '2017-12-01 23:25:02'),
(37, 7, '2017-12-01 23:25:11'),
(38, 8, '2017-12-01 23:25:20'),
(39, 9, '2017-12-01 23:25:47'),
(40, 11, '2017-12-01 23:25:55'),
(41, 14, '2017-12-01 23:26:13'),
(42, 13, '2017-12-01 23:26:24'),
(43, 2, '2017-11-24 00:00:00'),
(44, 2, '2017-11-26 11:24:24'),
(45, 2, '2017-11-28 16:31:25');

-- --------------------------------------------------------

--
-- 表的结构 `blxbgame_user`
--

CREATE TABLE `blxbgame_user` (
  `id` int(11) NOT NULL COMMENT '用户序号',
  `openid` varchar(250) NOT NULL COMMENT '用户openid',
  `nickname` varchar(10) NOT NULL COMMENT '用户昵称',
  `avatarUrl` varchar(150) DEFAULT NULL COMMENT '头像',
  `telephone` varchar(20) DEFAULT NULL COMMENT '电话',
  `createtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updatetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

--
-- 转存表中的数据 `blxbgame_user`
--

INSERT INTO `blxbgame_user` (`id`, `openid`, `nickname`, `avatarUrl`, `telephone`, `createtime`, `updatetime`) VALUES
(1, '976293711', 'wilson', 'https://', '15989785667', '2017-11-14 23:39:31', '2017-11-14 23:39:31'),
(2, 'ouof80FS1RePzeCpUAB28s5BYT48', 'wilson123', 'https://wx.qlogo.cn/mmopen/vi_32/Kyt8uAicHLT9PXKbErL2o8xOHGAznwBvP0FwciawX2RicA7axYUEpqzBgn7z0kTIGJKIa5uPiaREKy3UEUTPb5V6Vw/0', NULL, '2017-11-16 14:25:39', '2017-11-16 14:25:39'),
(3, '574845934', 'xiaoming', 'https://wx.qlogo.cn/mmopen/vi_32/Kyt8uAicHLT9PXKbErL2o8xOHGAznwBvP0FwciawX2RicA7axYUEpqzBgn7z0kTIGJKIa5uPiaREKy3UEUTPb5V6Vw/0', '13711650632', '2017-11-16 14:29:56', '2017-11-16 14:29:56'),
(4, '755327520', 'jj', 'https://wx.qlogo.cn/mmopen/vi_32/Kyt8uAicHLT9PXKbErL2o8xOHGAznwBvP0FwciawX2RicA7axYUEpqzBgn7z0kTIGJKIa5uPiaREKy3UEUTPb5V6Vw/0', '15978569554', '2017-11-16 14:29:56', '2017-11-16 14:29:56'),
(5, '66666', 'qweqwe', 'fdgfdg', NULL, '2017-12-01 23:24:24', '2017-12-01 23:24:24'),
(6, '53453fgfdg', '45435345', 'fdgfd', NULL, '2017-12-01 23:24:24', '2017-12-01 23:24:24'),
(7, '4235235', '2gfhdg', '4534534', NULL, '2017-12-01 23:24:24', '2017-12-01 23:24:24'),
(8, '465754645', '234324', '34534534', NULL, '2017-12-01 23:24:24', '2017-12-01 23:24:24'),
(9, '5465462435', '24525', '354634', NULL, '2017-12-01 23:24:24', '2017-12-01 23:24:24'),
(10, '4235fbnh', 'gfhb', '57656', NULL, '2017-12-01 23:24:24', '2017-12-01 23:24:24'),
(11, '657ugbgf', '34324', 'ghbg435', NULL, '2017-12-01 23:24:24', '2017-12-01 23:24:24'),
(12, '7yh654', '43gte', 'btreh', NULL, '2017-12-01 23:24:24', '2017-12-01 23:24:24'),
(13, '4hwew3', '436', '234t24', NULL, '2017-12-01 23:24:24', '2017-12-01 23:24:24'),
(14, '34t1wgve', '32423', '3yt45y4', NULL, '2017-12-01 23:24:24', '2017-12-01 23:24:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blxbgame_morning`
--
ALTER TABLE `blxbgame_morning`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userid` (`userid`),
  ADD UNIQUE KEY `id_time` (`userid`,`time`),
  ADD KEY `time_mse` (`time`,`msectime`),
  ADD KEY `time_mse_sco` (`time`,`msectime`,`totalscore`);

--
-- Indexes for table `blxbgame_morning_record`
--
ALTER TABLE `blxbgame_morning_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blxbgame_user`
--
ALTER TABLE `blxbgame_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `openid` (`openid`),
  ADD UNIQUE KEY `name` (`nickname`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `blxbgame_morning`
--
ALTER TABLE `blxbgame_morning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '起床表序号', AUTO_INCREMENT=13;
--
-- 使用表AUTO_INCREMENT `blxbgame_morning_record`
--
ALTER TABLE `blxbgame_morning_record`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT COMMENT '起床记录序号', AUTO_INCREMENT=46;
--
-- 使用表AUTO_INCREMENT `blxbgame_user`
--
ALTER TABLE `blxbgame_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户序号', AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
