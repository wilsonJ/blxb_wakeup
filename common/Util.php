<?php

namespace app\common;

use Yii; // 导入Yii

class Util{
 
	// 模拟登录
	public static function  LoginSchool($number,$password){
		$url_login = Yii::$app->params['URL']['LOGIN']; // 登录url
    	$post = array ( 
        	'userAccount'=>$number,
        	'userPassword'=>$password,
        	'encoded'=>base64_encode($number).'%%%'.base64_encode($password),
    	);    	// 提交的数据
        
    	$curl = curl_init();
    	curl_setopt($curl, CURLOPT_URL, $url_login); 
    	curl_setopt($curl, CURLOPT_HEADER, 1); // 是否显示头信息 
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 设定返回的数据是否自动显示
    	curl_setopt($curl, CURLOPT_POST, 1); // post方式提交 
    	curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post)); // 要提交的信息 
    	$result = curl_exec($curl); 
    	curl_close($curl);
    	
		$result = mb_convert_encoding($result, 'utf-8', 'gbk'); // 转换字符集
		$array_result = explode("\r\n\r\n", $result);  // 解析HTTP数据流 
		preg_match("/set\-cookie:([^\r\n]*)/i", $array_result[0], $matches);  // 解析COOKIE  
		$cookie = $matches[1];  // 获取cookie

		// 判断密码是否有误
		if($result){
			$partten = '/<font.*?color="red">用户名或密码错误<\/font>/';
			preg_match($partten, $result, $data);
			if($data){
				echo json_encode(array('error'=>2,'data'=>'学号和密码有误'));
				Yii::$app->end();
			}
		}
		return $cookie;
	}

	// post请求，发送数据，需要cookie
	public static function postDataByCookie($url, $cookie, $postData){
		$curl = curl_init(); 
		curl_setopt($curl, CURLOPT_URL, $url); 
		curl_setopt($curl, CURLOPT_HEADER, 0); // 是否显示头信息
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 设定返回的数据是否自动显示
		curl_setopt($curl, CURLOPT_POST, 1); // post提交
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postData)); //要提交的信息
		curl_setopt($curl, CURLOPT_COOKIE, $cookie); // 调取cookie信息
		$rs = curl_exec($curl);
		curl_close($curl); 
		return $rs; // 返回相应数据 
	}
	
	// get请求，需要cookie
	public static function getByCookie($url,$cookie){
		$curl = curl_init(); 
		curl_setopt($curl, CURLOPT_URL, $url); 
		curl_setopt($curl, CURLOPT_HEADER, 0); // 是否显示头信息
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 设定返回的数据是否自动显示
		curl_setopt($curl, CURLOPT_COOKIE, $cookie); // 调取cookie信息
		$rs = curl_exec($curl);
		curl_close($curl); 
		return $rs; // 返回相应数据
	}

	// get请求，SSL
    public static function getSSL($url){
		$curl = curl_init(); 
		curl_setopt($curl, CURLOPT_URL, $url); 
		curl_setopt($curl, CURLOPT_HEADER, 0); 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//这个是重点。
		$rs = curl_exec($curl);
		curl_close($curl); 
		return $rs; 
	}

    /**
     * curl
     * post 请求 SSL
     * @return json格式的文件
     */
	public static function postData($url,$data){
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1); // 从证书中检查SSL加密算法是否存在
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        if (curl_errno($curl)) {
            echo 'Errno'.curl_error($curl);//捕抓异常
        }
        curl_close($curl); // 关闭CURL会话
        return $tmpInfo; // 返回数据，json格式
    }

	/**
	 * [转换字符集，去除空白标签]
	 * @param  [type] $html_str [处理前数据]
	 * @return [type]           [处理后数据]
	 */
	static function deal_str($html_str){
		// 处理字符集
		$coding = mb_detect_encoding($html_str);
		if ($coding != "UTF-8" || !mb_check_encoding($html_str, "UTF-8"))
			$html_str = mb_convert_encoding($html_str, 'utf-8', 'GBK,UTF-8,ASCII');

		// 去除空标签
		$html_str = preg_replace("/[\t\n\r]+/", "", $html_str);

		return $html_str; // 返回数据
	}

	/**
	 * [去除指定标签]
	 * @param  [type] $tags [指定标签]
	 * @param  [type] $str  [处理数据]
	 * @return [type]       [description]
	 */
	static function strip_html_tags($tags,$str){ 
		$html=array();
		foreach ($tags as $tag) {
			$html[]="/(<(?:\/".$tag."|".$tag.")[^>]*>)/i";
		}
		$data=preg_replace($html, '', $str); 
		return $data;
	} 

}





