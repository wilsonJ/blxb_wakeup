<?php

return [
    'adminEmail' => 'admin@example.com',
	'URL' => [
		'LOGIN' => 'http://e.zhbit.com/jsxsd/xk/LoginToXk', // 登录
		'SCORE' => 'http://e.zhbit.com/jsxsd/kscj/cjcx_list', // 成绩
		'SCHEDULE' => 'http://e.zhbit.com/jsxsd/xskb/xskb_list.do', // 课表
		'EXAM' => 'http://e.zhbit.com/jsxsd/xsks/xsksap_list', // 考试
		'STINFO' => 'http://e.zhbit.com/jsxsd/grxx/xsxx', // 个人信息
		'OPENID' => 'https://api.weixin.qq.com/sns/jscode2session', // 获取openId
        'TOKEN' => 'https://api.weixin.qq.com/cgi-bin/token',//获取token
        'SEND_TEMPLATE' => 'https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send'
	],
	'WECHAT' => [
		'APPID' => 'wxa5dbe0c182610ebd', // appid
		'APPSECRET' => '90eb04b5a1adc5e9d48ccaf5c61405e5', // appsecret
	],
    'WakeUp'=> [
        'START' => '06:30:00',
        'OVER' => '10:30:00',
        'DATA'=> '2017-11-24',
    ],
    'Switch'=> [
    	'Breakfast'=> true,
    ],
];