<?php

namespace app\controllers;
use app\models\BlxbgameBonus;
use app\models\BlxbgameBonusRecord;
use app\controllers\basecommon\BaseController;
use app\models\BlxbgameUser;
use app\models\BlxbgameMorning;

use YII;

class BonusController extends BaseController
{

	public function actionIndex(){
		

		//$getInfo['openid'] = '976293711';
		//$this->actionAdd($getInfo);
        var_dump(BlxbgameMorning::TopTenbydatatime());
	}

    public function actionAdd()
    {
    	date_default_timezone_set('Asia/Shanghai');//设置时区
        $res = BlxbgameBonus::getTodayScore();

        if(empty($res)){
        	$content = '北鼻，当前时间为非活动时间  详细请关注公众号 ‘北理小报’ 了解更多详情';
        	$this->renderJSON($content);
        }



        $score = $res['totalscore']/100;
        $remain_score = $res['remain_score'];
        $YMDHISTime = date ( 'Y-m-d H:i:s' );

        

        if( YII::$app->params['Switch']['WakeUp'] && ($remain_score>0)  ){

        	//1.获取用户信息
        	$getInfo = Yii::$app->request->post();
        	$userInfo = $this->SearchInfoByOpenid($getInfo['openid']);
        	$morningResult = BlxbgameMorning::data_by_id($userInfo['id']);

        	//2.检测用户当天是否已经领取过奖池
        	if($this->judgeBonus($userInfo['id']))
        	{
        		$content = "北鼻，今天你已经领取积分，请再接再厉，明天约定你~\n\n";
        		$this->renderJSON($content);
        	}
        	//3.判断时间
        	 // $time = $this->judgeTime();
	         // if($time)
	         // {
	         //      $this->renderJSON($time);
	         // }

        	//4.增加积分 保存用户数据 更新数据库

	         $morningResult['totalscore']+=$score;
	         $remain_score -=$score;

        	//5.组织返回文字
	         if(BlxbgameBonus::update_socre($remain_score) && BlxbgameBonusRecord::addBonusRecord($userInfo,$score) && BlxbgameMorning::update_TotalScore($morningResult)  ){
	         	//更新剩余积分 增加记录 更新用户积分
	         	$title = "北鼻，你已成功开启奖池系统\n\n";
	         	$content=$title."恭喜你获得".$score."积分,系统已为你累加 刷新即查询哦\n\n";
	         	$this->renderJSON($content);
	         }else {
            $content = "领取失败,后台@留言,联系‘北理小报’~";
             $this->renderJSON($content);
           }

        }
        else{
        	$content = "\n到".$YMDHISTime."为止,当天奖池积分已派送完毕，亲爱的北鼻，不要气馁，明天再接再厉";
        	$this->renderJSON($content);
        }


        //return $this->render('index');
    }

    public function judgeTime(){
        date_default_timezone_set('Asia/Shanghai');
        $HISTime = date ( 'H:i:s' ); // 用户当天起床的时间的 与设置时间做对比
        $BounsStartTime = YII::$app->params['BounsTime']['START'];
        $BounsOverTime = YII::$app->params['BounsTime']['OVER'];
        if( $HISTime >= $BounsStartTime && $HISTime < $BounsOverTime){
            // 20:30:00-21:00:00
            return FALSE;
        }else {
        	return "北鼻,现在还不是奖池开放时间,奖池开放时间为每天的20:30:00-21:00:00\n";
        }

    }

    public function judgeBonus($userid){

       if (BlxbgameBonusRecord::judgeRecord($userid))
       	     return true;
        else 
            return false;

    }

}
