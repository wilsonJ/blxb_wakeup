<?php

namespace app\controllers;
use app\controllers\basecommon\BaseController;
use app\models\BlxbgameUser;
use app\models\BlxbgameMorning;
use app\models\BlxbgameMorningRecord;
use YII;

class RankController extends BaseController
{
    public function actionIndex()
    {
        //$userInfo['openid'] = '574845934';

        //$this->actionRankforten();
        $Topten_score = BlxbgameMorning::TopTenbyScore();
        var_dump($Topten_score);
        //return $this->render('index');
    }

    public function actionGetuserinfo(){

    	 //首先检查今天有没有签到

        $getInfo = Yii::$app->request->post();
        //$OpenId  = $postData['openid']
        $userInfo = $this->SearchInfoByOpenid($getInfo['openid']);
        $morningResult = BlxbgameMorning::data_by_id($userInfo['id']);
        $morningRank = $this->getMorningRank($morningResult['time'],$morningResult['msectime']);
        $totalscoreRank = $this->getTotalScoreRank($morningResult['totalscore'],$morningResult['time'],$morningResult['msectime']);
        //var_dump($morningResult['time']);
        $time = explode(' ',$morningResult['time']);
        preg_match('/[0-9][0-9]:[0-9][0-9]/', $time['1'],$matches);
        $time = $matches['0'];
        $data = array(
            'time'=>$time,
            'totalscore'=>$morningResult['totalscore'],
            'signdays' =>$morningResult['signdays'],
            'morningRank'=>$morningRank,
            'totalscoreRank'=>$totalscoreRank,
            'morningRank'=>$morningRank
        );
        $this->renderJSON($data);
    }

    public function actionRankforten()
    {
        $Topten = BlxbgameMorning::TopTenbydatatime();
        $Lastten = BlxbgameMorning::LastTenbydatatime();
        $Topten_score = BlxbgameMorning::TopTenbyScore();
        $data = array(
            'Topten'=>$Topten,
            'Lastten'=>$Lastten,
            'Topten_score'=>$Topten_score,
        );
         $this->renderJSON($data);
    }
        //计算签到的第几天
    public function actionTotalsignup(){

        date_default_timezone_set('Asia/Shanghai');
        $today_data = date ( 'Y-m-d' );

        //$userid = '25';
        $getInfo = Yii::$app->request->post();
        $userInfo = $this->SearchInfoByOpenid($getInfo['openid']);
        $userid = $userInfo['id'];
        $res = BlxbgameMorningRecord::getWakeUpData($userid);
        $start = YII::$app->params['WakeUp']['DATA'];
        $start_data = explode('-',$start);
        $today_data = explode('-',$today_data);
        //var_dump($start_data['2']);

        if($start_data['1'] ==$today_data['1']){
            $today = $today_data['2']-$start_data['2'];
        }else if( ($start_data['1']+1) == $today_data['1'] ){
             $start_data['2'] = 1;
            $today = $today_data['2']-$start_data['2']+7;
        }
       
        //var_dump($today);
        $start = YII::$app->params['WakeUp']['DATA'];
        $start_data = explode('-',$start);

        for($i=0;$i<$today;$i++){
            $arr[$i] = 0;
        }

        foreach ($res as $value){
            $time = explode(' ',$value['time']);
            $time = $time['0'];
            $time = explode('-',$time);//获得具体时间
            if($start_data['0'] ==$time['0'] &&  $start_data['1'] ==$time['1'] ) {

                $res = $time['2'] - $start_data['2'];
               
                $arr[$res] = 1;
            }else if( ($start_data['1']+1) == $time['1'] ){
                $start_data['2'] = 1;
                $res = $time['2'] - $start_data['2']+7;
                //echo $res;
                $arr[$res] = 1;
            }
            $data = array(
            'today'=>$today,
            'isSignUp'=>$arr,
              );   

            //var_dump($time);
            //var_dump($res);
        }
         $this->renderJSON($data);
    }

}
