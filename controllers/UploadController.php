<?php

namespace app\controllers; //命名空间

use yii\web\Controller; // 导入Controller
use Yii; // 导入Yii
use app\models\UploadFileForm;  // 文件模型
use yii\web\UploadedFile;  // 引入上传插件
use crazyfd\qiniu\Qiniu;  // 引入七牛插件


// CommunityController 控制器类
class UploadController extends Controller{
	

	/**
	* 
	*/
	public function actionIndex(){

	}

	/**
	* 通过7牛云存储 上传图片
	*/
	public function actionUploadpic(){
		if(Yii::$app->request->isPost){
			$model = new UploadFileForm();
			$model->file = UploadedFile::getInstance($model, 'file');
			if(empty($model->file)){
				echo json_encode( array('error'=>1,'data'=>'没有提交数据') );
			}
			$qiniu = new Qiniu(UploadFileForm::AK,UploadFileForm::SK,UploadFileForm::DOMAIN,UploadFileForm::BUCKET);
			$key = 'blxb/'.uniqid();
			$qiniu->uploadFile($model->file->tempName,$key);
			$link = $qiniu->getLink($key);
			echo json_encode( array('error'=>0,'data'=>$link) );
		}else{
			echo json_encode( array('error'=>1,'data'=>'提交数据方式有误') );
		}
	}


}




