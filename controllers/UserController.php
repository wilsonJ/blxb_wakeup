<?php

namespace app\controllers; //命名空间

use yii\web\Controller; // 导入Controller
use Yii; // 导入Yii
use app\common\Util;  // 导入公共方法
use app\models\User; // 用户模型


// UserController 控制器类
class UserController extends Controller{
 	
	 // 爬虫 教务的个人信息
	 public function actionIndex(){
			
		// 检查提交方式
		if(Yii::$app->request->isPost){
			// 获取数据
			$postData = Yii::$app->request->post();

			// 检查缓存
			$result = Yii::$app->redis->keys($postData['number'].'_'.$postData['type'].'_*'); // 获取缓存数据
			$time = NULL;
			if($result){
				$array_result = explode('_',$result[0]); // 获取缓存写入时间
				$time = time() - intval($array_result[2]);// 计算相隔时间
			}
		    $unUpdate = $time < 500 ? TRUE : FALSE; // 判断是否需要更新，500秒自动更新
					
			if( $result && $unUpdate ){
				// 缓存存在数据 且 不需要更新
				$data = Yii::$app->redis->get($result[0]);
				$data = json_decode($data,true);
				echo json_encode( array('error'=>0,'data'=>$data) );
			}else{
				// 获取新的数据
				$cookie = Util::LoginSchool($postData['number'],$postData['password']); // 模拟登录
				$data = $this->getHtmlStInfo($cookie); // 获取个人信息
				
				// 判断是否获取数据成功
				if($data){ 
					Yii::$app->redis->del(@$result[0]); // 删除缓存数据
				}else{
					$data = Yii::$app->redis->get($result[0]); // 获取之前的缓存
				}			
				
				// 写入缓存（更新数据/更新最新时间）
				if( $data && !Yii::$app->redis->set( $postData['number'].'_'.$postData['type'].'_'.time(), json_encode($data) ) ){
					Yii::info($postData['number'].'_'.$postData['type'].'____写入缓存失败', 'redis');
				}
				// 返回数据			
				echo json_encode( array('error'=>0,'data'=>$data) );
			}

		}else{
			echo json_encode( array('error'=>1,'data'=>'提交数据方式有误') );
		}
	}

	public function getHtmlStInfo($cookie){

		$url_StInfo = Yii::$app->params['URL']['STINFO']; // 个人信息url
		$html_StInfo = Util::getByCookie($url_StInfo, $cookie); // 提交请求

		$html_StInfo = Util::deal_str($html_StInfo); // 格式化返回结果
		$html_StInfo = Util::strip_html_tags(array('br'),$html_StInfo); //删除<br>标签
		

		$data = array();

		// 学院 专业 学号
		$partten_1 = '/<td align="center"  style="border:0px solid black"  colspan="2">(.*?)<\/td>/';
		preg_match_all($partten_1, $html_StInfo, $dataPartten);

		$data = $this->dealData($data, 'college', $dataPartten[1][0]);
		$data = $this->dealData($data, 'major', $dataPartten[1][1]);
		$data = $this->dealData($data, 'number', $dataPartten[1][2]);

		// 学制 班级
		$partten_2 = '/<td align="center"  style="border:0px solid black" >(.*?)<\/td>/';
		preg_match_all($partten_2, $html_StInfo, $dataPartten);

		$data = $this->dealData($data, 'length', $dataPartten[1][0]);
		$data = $this->dealData($data, 'class', $dataPartten[1][1]);
		
		return $data;
	}
	
	public function dealData($newData, $name, $data){
		$tempData = explode("：", $data);
		$newData[$name] = $tempData[1];
		return $newData;
	}

	public function actionGetnupabyopenid(){
		
		if(Yii::$app->request->isPost){
			$postData = Yii::$app->request->post(); 
			
			$result = Yii::$app->redis->keys($postData['openId'].'_*'); // 获取缓存数据
			$time = NULL;
			if($result){
				$array_result = explode('_',$result[0]); // 获取缓存写入时间
				$time = time() - intval($array_result[1]);// 计算相隔时间
			}
		    $unUpdate = $time < 5 ? TRUE : FALSE; // 判断是否需要更新，500秒自动更新
					
			if( $result && $unUpdate ){
				// 缓存存在数据 且 不需要更新
				$data = Yii::$app->redis->get($result[0]);
				$data = json_decode($data,true);
				echo json_encode( array('error'=>0,'data'=>$data) );
			}else{
				// 获取新的数据
				$model = new User;	
				$data = $model->getNuPaByOpenId($postData['openId']);
				
				// 判断是否获取数据成功
				if($data){ 
					Yii::$app->redis->del(@$result[0]); // 删除缓存数据
				}else{
					$data = Yii::$app->redis->get(@$result[0]); // 获取之前的缓存
				}			
				
				// 写入缓存（更新数据/更新最新时间）
				if( $data && !Yii::$app->redis->set( $postData['openId'].'_'.time(), json_encode($data) ) ){
					Yii::info($postData['number'].'____写入缓存失败', 'redis');
				}
				
				// 返回数据			
				if($data){
					echo json_encode( array('error'=>0,'data'=>$data) );
				}else{
					echo json_encode( array('error'=>1,'data'=>'暂时无法提供数据，请联系小程序管理员') );
				}
			}
		}else{
			echo json_encode( array('error'=>1,'data'=>'提交数据方式有误') );
		}
	}

	// 绑定/更新
	public function actionBind(){
		
		if(Yii::$app->request->isPost){

			$model = new User;	
			$postData = Yii::$app->request->post(); // 获取数据
			
			// 检查账号和密码有无错误
			Util::LoginSchool($postData['number'],$postData['password']); // 模拟登录
			
			// 防止用户重复提交数据，先对比数据库之前的数据
			$data = $model->getNuPaByOpenId($postData['openId']);	
			if( $data && $data['number'] == $postData['number'] && $data['password'] == $postData['password']){
				echo json_encode( array('error'=>0,'data'=>'已经保存成功了哦~') );	
				Yii::$app->end();
			}

		    // 检查之前是否有资料，进行更新还是插入操作
			if($data){
				// 数据库已经存在资料，需要更新
				$sqlResult = $model->updateNuPa($postData);
			}else{
				// 数据不存在资料，需要插入
				$sqlResult = $model->insertNuPa($postData);
			}

			// 写缓存
			if($sqlResult){	
				$result = Yii::$app->redis->keys($postData['openId'].'_*'); // 获取缓存数据
				if($result){		
					Yii::$app->redis->del($result[0]); // 删除缓存数据	
				}
				// 写入缓存（更新数据/更新最新时间）
				if( !Yii::$app->redis->set( $postData['openId'].'_'.time(), json_encode($postData) ) ){
					Yii::info($postData['number'].'____写入缓存失败', 'redis');
				}
			}

			// 返回信息
			if($sqlResult){
				echo json_encode( array('error'=>0,'data'=>'更新数据成功') );	
			}else{
				echo json_encode( array('error'=>1,'data'=>'更新数据失败') );
			}
		}else{
			echo json_encode( array('error'=>1,'data'=>'提交数据方式有误') );
		}
	}

	// 解绑
	public function actionUnbind(){
		
		if(Yii::$app->request->isPost){

			$model = new User;	
			$postData = Yii::$app->request->post(); // 获取数据
			
			// 判断用户之前是不是绑定过，预防不测
			$data = $model->getNuPaByOpenId($postData['openId']);	
			if( !$data ){
				echo json_encode( array('error'=>1,'data'=>'绑定了才有解绑功能哦~') );	
				Yii::$app->end();
			}

			// 进行解绑
			$sqlResult = $model->updateState($data);
	
			$result_1 = Yii::$app->redis->keys($postData['openId'].'_*'); // 获取相关缓存数据
			$result_2 = Yii::$app->redis->keys($data['number'].'_*'); // 获取相关缓存数据
			if( $sqlResult && ($result_1 || $result_2) ){		
				for($i=0; $i<count($result_1); $i++){
					Yii::$app->redis->del($result_1[$i]); // 删除缓存数据
				}
				for($i=0; $i<count($result_2); $i++){
					Yii::$app->redis->del($result_2[$i]); // 删除缓存数据
				}
			}
			
			// 返回提示
			if($sqlResult){
				echo json_encode( array('error'=>0,'data'=>'解绑成功') );	
			}else{
				echo json_encode( array('error'=>1,'data'=>'解绑失败') );
			}

		}else{
			echo json_encode( array('error'=>1,'data'=>'提交数据方式有误') );
		}

	}
}


