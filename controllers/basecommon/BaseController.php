<?php
/**
 * 公用函数类
 * Class BaseController
 * @author wilson
 */

namespace app\controllers\basecommon;

use app\models\BlxbgameUser;
use Yii;
use yii\web\Controller;
use app\models\BlxbgameMorning;
use app\models\BlxbgameMorningRecord;


//是以后所有控制器的基类，并且集成常用公用方法
class BaseController extends  Controller
{
	
	/*
	 * 封装json返回值
	 * error: 0 无误， -1 提交方式错误  1 查找不到数据
	 * data: 此次返回的数据
	 */
	public function renderJSON($data=[], $error=0){
		header('Content-type: application/json'); // 设置头部内容格式
		echo json_encode([
			'error'=>$error,
			'data'=>$data
		]);
		return Yii::$app->end(); // 终止请求直接返回
	}

	/*
	 *  判断是不是Post请求
     *  不是Post请求，直接中断请求
	 */
	public function judgePostMethod(){
		if( !Yii::$app->request->isPost ){
			return $this->renderJSON('提交方式错误',-1);
		}
		return;
	}


    /**
     * 检查openId 是否在数据库中
     * openid 存在 返回true
     *         不存在 返回 false
     */
	public function judgeOpenId($openid){
        $res = BlxbgameUser::judgeOpenId($openid);
        //var_dump($res);
        if($res)
            return true;
        else
            return false;
    }
	
	/*
	 *  判断数据是不是为空
     *  是空数据，直接中断请求
	 */
	public function judgeDataEmpty($data){
		if( empty($data) ){
			return $this->renderJSON('查找不到数据',1);
		}
		return;
	}
	
	/*
	 *  判断数据是不是为真
     *  为真，返回执行成功，为假，返回执行错误
	 */
	public function judgeDataTrue($data){
		if($data){
			return $this->renderJSON('执行成功');
		}else{
			return $this->renderJSON('执行失败',2);
		}
	}

    public function SearchInfoByOpenid($openid){
	    $model = new BlxbgameUser();
	    $res = $model->data_by_openid($openid);
	    if ($res){
	        return $res;
        }
        else{
	        return FALSE;
        }

    }

	public function SearchInfo($openid){
        $result = Yii::$app->redis->keys("info_".$openid); // 检查缓存中 有木有数据

        if($result){
            $userResult = Yii::$app->redis->get("info_".$openid);
        }else{
            //从user表中查找
            $userResult = null;

            if($userResult)
                $source = Yii::$app->redis->setex("info_".$openid,'3600',$userResult);
        }

        if($userResult){
            return $userResult;
        }else{
            return false;
        }
    }

    /**
     * [判断是否是星期六日]
     * @return 1-5 true  / 6-7 false
     */
    public function judgeWeekend(){
        $week = date("w");
        if($week==0 || $week==6){
            $weekday = false;//6-7 false
        }else{
            $weekday = true;//1-5 true
        }
        return $weekday; //区分周六日
    }

    /**
     * 根据当前时间获取问候语
     * @return string
     */
    public function getGreetWord(){
        date_default_timezone_set('Asia/Shanghai');
        $HISTime = date ( 'H:i:s' );
        // 问候语句
        if( $HISTime >= 6 && $HISTime < 12){
            $greetWorld="早上好";
        }else if( $HISTime >= 12 && $HISTime < 19){
            $greetWorld="下午好";
        }else if( $HISTime >= 19 && $HISTime < 24){
            $greetWorld="晚上好";
        }else if( $HISTime >= 0 && $HISTime < 6){
            $greetWorld="凌晨好";
        }
        return $greetWorld;
    }

    public function getMorningRank($time,$msectime){
        if($time == 0){
            return 0;
        }
        $rank =BlxbgameMorning::getMorningRank($time,$msectime);
        return $rank;
    }
    public function getTotalScoreRank($totalscore,$time,$msectime){
        if($time == 0){
            return 0;
        }
        $rank = BlxbgameMorning::getTotalScoreRank($totalscore,$msectime);
        return $rank;

    }




	
    
    
}


