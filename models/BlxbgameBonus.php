<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blxbgame_bonus".
 *
 * @property integer $bonus_id
 * @property integer $totalscore
 * @property integer $remain_score
 * @property string $datetime
 */
class BlxbgameBonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blxbgame_bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['totalscore', 'remain_score', 'datetime'], 'required'],
            //[['totalscore', 'remain_score'], 'integer'],
            [['datetime'], 'safe'],
            [['datetime'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bonus_id' => '序号',
            'totalscore' => '总奖池',
            'remain_score' => '剩余积分',
            'datetime' => '开奖日期',
        ];
    }

    public function getTodayScore(){
        date_default_timezone_set('Asia/Shanghai');
        $today = date ( 'Y-m-d' );
        $data = BlxbgameBonus::find()->where('datetime = :datetime',[':datetime'=>$today])->asArray()->one();
        if($data){
           return $data;
        }
        else{
            return false;
        }
    }

    public static function update_socre( $remain_score){
        date_default_timezone_set('Asia/Shanghai');
        $today = date ( 'Y-m-d' );
        $data = BlxbgameBonus::find()->where('datetime = :datetime',[':datetime'=>$today])->one();
        
         $data->remain_score=$remain_score;
        if ($data->save()) {
            return true;
        }
        return false;
    }
}
