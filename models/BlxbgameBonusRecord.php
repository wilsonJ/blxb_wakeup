<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blxbgame_bonus_record".
 *
 * @property integer $id
 * @property integer $userid
 * @property integer $gain_socre
 * @property string $time
 */
class BlxbgameBonusRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blxbgame_bonus_record';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'gain_socre'], 'required'],
            [['userid', 'gain_socre'], 'integer'],
            [['time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '序号',
            'userid' => '用户序号',
            'gain_socre' => '获得的积分',
            'time' => '时间',
        ];
    }

    public static function judgeRecord($userid){
        date_default_timezone_set('Asia/Shanghai');
        $today = date ( 'Y-m-d' );
        $today.='%';
        $data = BlxbgameBonusRecord::find()->where('time LIKE :time AND  userid = :userid',[':time'=>$today,':userid'=>$userid])->asArray()->one();
        if($data)
            return true;
        else 
            return false;

    }

    public static function addBonusRecord($userInfo,$score){
        $data = array(
            'userid'=>$userInfo['id'],
            'gain_socre'=>$score
        );
        $model = new BlxbgameBonusRecord();
        if ($model->load($data,'') && $model->save()) {
            return true;
        }
        return false;
    }
}
