<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blxbgame_morning_record".
 *
 * @property integer $id
 * @property integer $userid
 * @property string $time
 */
class BlxbgameMorningRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blxbgame_morning_record';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid'], 'required'],
            [['userid'], 'integer'],
            [['time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '起床记录序号',
            'userid' => '用户序号',
            'time' => '签到时间',
        ];
    }

    public static function addMorningRecord($userInfo){
        date_default_timezone_set('Asia/Shanghai');
        $time = date ( 'Y-m-d' );
        $data = array(
            'userid'=>$userInfo['id'],
        );
        $model = new BlxbgameMorningRecord();
        if ($model->load($data,'') && $model->save()) {
            return true;
        }
        $log_content = "新增morningrecord表(起床记录表)失败，无法修改起床时间，连续签到时间|用户信息：".var_export($data, true)."\n";
        $log_content.= $model->createCommand()->getRawSql()."\n";
        Yii::warning($log_content);
        return false;
    }

    public static function getWakeUpData($userid){

        $data =   BlxbgameMorningRecord::find()->where('userid = :userid  ',[':userid'=>$userid])->orderBy('time asc')->asArray()->all();
        return   $data;
    }


}
