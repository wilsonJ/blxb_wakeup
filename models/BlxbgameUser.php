<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blxbgame_user".
 *
 * @property integer $id
 * @property string $openid
 * @property string $nickname
 * @property string $avatarUrl
 * @property string $telephone
 * @property string $createtime
 * @property string $updatetime
 */
class BlxbgameUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blxbgame_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['openid', 'nickname'], 'required'],
            [['createtime', 'updatetime'], 'safe'],
            [['openid', 'avatarUrl'], 'string', 'max' => 250],
            [['nickname'], 'string', 'max' => 25],
            [['telephone'], 'string', 'max' => 20],
            [['openid'], 'unique'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '用户序号',
            'openid' => '用户openid',
            'nickname' => '用户昵称',
            'avatarUrl' => '头像',
            'telephone' => '电话',
            'createtime' => '创建时间',
            'updatetime' => '更新时间',
        ];
    }

    public static function judgeOpenId($openid){
        $data = BlxbgameUser::find()->where('openid = :openid',[':openid'=>$openid] )->asArray()->one();
        return $data;
    }

    public static function data_by_openid($openid){
        $data = BlxbgameUser::find()->where('openid = :openid',[':openid'=>$openid] )->asArray()->one();;
        return $data;
    }

    public static function data_by_id($userid){
        $data =  BlxbgameUser::find()->where('id = :userid',[':userid'=>$userid] )->asArray()->one();
        return $data;
    }

    public static function add($data)
    {
        $model = new BlxbgameUser();
        if ($model->load($data,'') && $model->save()) {
            return true;
        }
        return false;
    }
}
